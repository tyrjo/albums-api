# Albums API

A simple API for music album data. Serves on all interfaces on port 5000 (or `PORT` environment variable).
Includes basic API documentation and an API browser.

Endpoints:
See [src/swagger.yml](src/swagger.yml)
* `GET /` - Swagger UI showing minimal API documentation
* `GET /albums` - list of albums including artist id, genre name, album title, album year and album id
* `GET /albums/<integer id>` - get single album by id
* `POST /albums` - add album. Expects JSON body containing `artist` string, `genre` string, `album` (title) string, `year` string
* `PUT /albums/<integer id>` - overwrite specific album. Expects JSON body matching POST format above.
* `DELETE /albums/<integer id>` - remove specific album.
* `GET /artists` - list of artists including artist id and artist name
* `GET /artists/<integer id>` - get single artist by id
* `GET /artists/<integer id>/albums` - get all albums for single artist id
* `GET /rankedgenres` - list of genres including genre name, count of albums in that genre
* `GET /mostpopularyears` -

# Limitations
* Update replaces existing records. All fields are required.
* Albums are not deduplicated
* Artists may exist that no longer are associated with any albums
* Update or delete of non-existent album ID returns no error

## Usage Examples
See the `curl` examples in [tests](tests).

# Open Questions
* How would you like to handle special characters in the input data?
* Are genre's case sensitive (assuming Yes)
* Would you like update to replace or modify existing albums?
* Database concurrency
* SQL injection protected?

# Requirements
* Include a README or text file with instructions to set up the project
* Given [albums.csv](docs/albums.csv) create an API that:
   * Creates an album
   * Reads an album
   * Updates an album
   * Deletes an album
   * Returns an index of artists
   * Given an artist returns a list of artist's albums
   * Returns genres ranked by number of albums
   * Returns years with the most albums
* Submit a zip file of the code

# Installation

## Requirements
* Python 2.7.14 or greater
* pip 18.1 or greater
* (Optional) virtualenv

## Steps
```
    # From GitHub
    git clone git@gitlab.com:tyrjo/albums-api.git
```
```
    # From zipfile
    unzip albums-api-<version>.zip -d albums-api
```
```
    cd albums-api
    
    # Optional: Create a virtual environment:
    virtualenv --python=python2.7 .venv
    . .venv/bin/activate
    
    pip install -r requirements.txt
    sqlite3 database.db < init_db.sql
    python ./src/Loader.py
    python ./src/server.py
```
Connect a web browser to `http://<your ip>:5000`

# Manual Test Plan
See example scripts in [tests](tests). Set HOST and PORT environment variables first.

## Setup
* PASS - albums.csv is valid 4 column data
* PASS - setup steps complete

## Data Loading
*  PASS - loaded data matches CSV file
    *  PASS - 103 items
    *  PASS - all items have data in all colums
    *  PASS - special characters preserved or appropriately converted
*  PASS - loaded data excludes header row

## CRUD operations
*  PASS - GET /albums returns all albums
*  PASS - GET /albums includes album IDs
*  PASS - GET /albums/<id> returns specific album data
*  PASS - GET /albums/<invalid id> returns empty
   * PASS - 0
   * PASS - snarf (404)
   * PASS - 104
*  PASS - POST /albums adds new album
*  PASS - POST /albums returns ID of added album
*  PASS - GET after POST includes new album data
*  PASS - PUT /albums/<id> replaces existing album
*  PASS - PUT /albums/<invalid id> ignores put
   * PASS - 0
   * PASS - 104
*  PASS - GET after PUT includes modified album data
* PASS - DELETE /albums/<id> removes an album
* PASS - DELETE /albums/<invalid id> ignores delete
   * 0
   * snarf
   * 104
* PASS - GET after DELETE no longer included deleted album
* PASS - GET /albums/2 after DELETE /albums/1 returns correct album

# Artist index
* PASS - GET /artists returns unique list of artists
* PASS - GET /artists includes artist IDs
* PASS - GET /artists/<id>/albums returns all albums for given artist id
* PASS - GET /artists results change as data added

# Genres
* PASS - GET /rankedgenres returns unique list of genres
* PASS - GET /rankedgenres sorted by album count for genre
* PASS - GET /rankedgenres includes album count per genre
* PASS - GET /rankedgenres results change as data added or removed

# Years
* PASS - GET /mostpopularyears returns unique list of years that have the most albums
* PASS - GET /mostpopularyears returns 1 item when 1 year has most albums
* PASS - GET /mostpopularyears returns multiple years when multiple years have same "most" albums
* PASS - GET /mostpopularyears results change as data added or removed

# Developer Notes
A hot-reloading development server is available via `npm`. The server will
restart on any changes in the src directory.
* `npm install`
* `npm start`

To build a new version (zipfile in `dist/`)
* `npm run build`