import csv
import json
import sqlite3


class Loader:
    
    def load(self, filepath):
        file = open(filepath, 'r')
        reader= csv.DictReader( file, strict=True)
        db = sqlite3.connect('database.db')
        db.text_factory = str
        c = db.cursor()
        id = 1
        for row in reader:
            c.execute('insert or ignore into artists (name) values (?)', (row['artist'],) )
            c.execute('insert into albums (title, artist, genre, year) values(?, (select id from artists where name=?), ?, ?)', (row['album'], row['artist'], row['genre'], row['year']) )
        db.commit()
        db.close()
        file.close()
        return

if __name__ == '__main__':
    loader = Loader()
    loader.load('./data/albums.csv')
