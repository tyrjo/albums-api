from flask import Flask
from flask import request
from flask import jsonify
from flasgger import Swagger

import os
import json
import sqlite3

app = Flask(__name__)
swagger_config = {
    "headers": [
    ],
    "specs": [
        {
            "endpoint": 'albums-api',
            "route": '/albums-api.json',
        }
    ],
    "static_url_path": "/flasgger_static",
    "swagger_ui": True,
    "specs_route": "/"
}
swagger = Swagger(app, template_file='swagger.yml', config=swagger_config)

def query(sql, params=()):
    db = sqlite3.connect('database.db')
    db.text_factory = str
    c = db.cursor()
    rows = c.execute(sql, params)
    column_names = [key[0] for key in c.description]
    results = [dict(zip(column_names, row)) for row in rows]
    db.commit()
    db.close()
    return results
    
def execute(sql, params=()):
    db = sqlite3.connect('database.db')
    db.text_factory = str
    c = db.cursor()
    result = c.execute(sql, params)
    db.commit()
    db.close()
    return result

@app.route('/albums', methods=['POST'])
def create_album():
    data = request.get_json()
    execute('insert or ignore into artists (name) values (?)', (data['artist'],) )
    execute('insert into albums (title, artist, genre, year) values(?, (select id from artists where name=?), ?, ?)', (data['album'], data['artist'], data['genre'], data['year']) )
    return jsonify({
        "results": []
    })

@app.route('/albums', methods=['GET'])
def read_albums():
    results = query('select * from albums')
    return jsonify({
        "results": results
    })
    
@app.route('/albums/<int:id>', methods=['GET'])
def read_album(id):
    results = query('select * from albums where id=?', (id,))
    return jsonify({
        "results": results
    })

@app.route('/albums/<int:id>', methods=['PUT'])
def update_album(id):
    data = request.get_json()
    execute('insert or ignore into artists (name) values (?)', (data['artist'],) )
    result = execute('update or fail albums set title=?,artist=(select id from artists where name=?),genre=?,year=? where id=?', (data['album'], data['artist'], data['genre'], data['year'], id) )
    return jsonify({
        "results": []
    })

@app.route('/albums/<int:id>', methods=['DELETE'])
def delete_album(id):
    data = request.get_json()
    execute('delete from albums where id=?', (id,) )
    return jsonify({
        "results": []
    })

@app.route('/artists', methods=['GET'])
def read_artists():
    results = query('select * from artists')
    return jsonify({
        "results": results
    })
    
@app.route('/artists/<int:id>', methods=['GET'])
def read_artist(id):
    results = query('select * from artists where id=?', (id,))
    return jsonify({
        "results": results
    })
    
@app.route('/artists/<int:id>/albums', methods=['GET'])
def read_artist_albums(id):
    results = query('select * from albums where artist=?', (id,))
    return jsonify({
        "results": results
    })
    
@app.route('/rankedgenres', methods=['GET'])
def read_rankedgenre():
    results = query('select count() as count,genre from albums group by genre order by count() desc')
    return jsonify({
        "results": results
    })
    
@app.route('/mostpopularyears', methods=['GET'])
def read_popularyear():
    # in SQL it would be:
    # select yearcount,year
    # from (select count() as yearcount,year from albums group by year order by count() desc)
    # where yearcount=(select max(yearcount) from (select count() as yearcount from albums group by year))
    results = query('select count() as count,year from albums group by year order by count() desc')
    maxCount = max([r['count'] for r in results])
    results = [r for r in results if r['count']==maxCount]
    print(maxCount)
    return jsonify({
        "results": results
    })
    
if __name__ == '__main__':
    app.run(port=os.environ.get('PORT', 5000), host='0.0.0.0');